/**
 * Created by Developer on 13.06.2017.
 */
$(window).ready(function() {


    //Календарь-----------------------------------------------------------------
    $(function() {
        if ($('.calendar').length > 0) { // если есть такой такой элемент на странице то больше не проверять его
            $('.calendar').pignoseCalendar({
                multiple: true,
                week: 1
            });
        }
    });
    //Конец Календарь-----------------------------------------------------------

    //Выделение всех чеков------------------------------------------------------

    $('.sel_all').change(function() {
        if (!$('.sel_all').is(":checked")) {
            $(".list_tele_mentor input, .new_obrashenie input").removeAttr("checked");
        } else {
            $(".list_tele_mentor input, .new_obrashenie input").prop("checked", "checked");

        }
    });

    //Конец Выделение всех чеков------------------------------------------------

    //новое обращение-----------------------------------------------------------

    $('.add_obrashenie').click(function() {
        $('.new_obrashenie').toggleClass('active');
    });
    // jQuery(function($) {
    //     $(document).mouseup(function(e) {            // событие клика по веб-документу
    //         e = $(e.target);
    //         var new_obrashenie = 'new_obrashenie';   // тут указываем класс элементов
    //         if (!e.hasClass(new_obrashenie)          // если клик был не по нашему блоку
    //             &&
    //             !e.parents('.' + new_obrashenie).length) {     // и не по его дочерним элементам
    //             $('.' + new_obrashenie).removeClass('active'); // скрываем их
    //         }
    //     });
    // });

    // Конец новое обращение---------------------------------------------------------------


    //Виджет да или нет------------------------------------------------------------
    function get_que_string() {
        $('.que__widget').each(function() {
            $(this).find('button').click(function() {
                var $btn_val = $(this).attr('data-val');
                $(this).parent().find('input').val($btn_val);
            });
        });
    }
    get_que_string();
    //Конец Виджет да или нет------------------------------------------------------------



    // Аккордеон----------------------------------------------------------------

    ! function(i) {
        var o, n;
        //объявляем переменные
        i(".title_block").on("click", function() {
        //функция i - это клик по ".title_block"
            o = i(this).parents(".accordion_item"),
            //находит предков ".accordion_item" кликом по ".title_block" и присваивает в = о
            n = o.find(".info"),
            //ищет в ".accordion_item" блок ".info" и переприсваивает его в переменную = n
                o.hasClass("active_block") ? (o.removeClass("active_block"),
                //проверяет у ".info" наличие класса "active_block" но у остальных ".info" удаляет "active_block"
                    n.slideUp()) : (o.addClass("active_block"), n.stop(!0, !0).slideDown(),
                    //сворачивает элемент n ".info" т.е. делает display:none и раскрывает тот которому добавляется класс "active_block" т.е. выбранному
                    o.siblings(".active_block").removeClass("active_block").children(".info").stop(!0, !0).slideUp())
                    //в DOM дереве находит класс ".active_block" и удаляет его у ".info"
        })
    }(jQuery);

    //Конец Аккордеон-----------------------------------------------------------


    //вызов календаря-----------------------------------------------------------
    $('.data').click(function() {
        $('.calendar').toggleClass('active');
    });
    // конец вызов календаря----------------------------------------------------

    // Вызов модалки------------------------------------------------------------
    $('.modal_btn, .close, .cancel').click(function() {
        $('.modal_bg').toggleClass('active');
    });
    ///////------------ очистка инпутов при закрытии модалки--------------------

    $('.close, .cancel').click(function() {
        $('.settings_pas input').val('');
        $('.error2').removeClass('active').fadeOut(200);
        $('#pass1, #pass2').css('border', '#dce0e4 1px solid');

    });

    // Конец Вызов модалки------------------------------------------------------

    //форма_валидация ----------------------------------------------------------

    $(".settings_pas input").keyup(function() {
        var pass_1 = $('#pass1').val();
        var pass_2 = $('#pass2').val();
//        $('#pass2').blur(function () {
            if (pass_1 === pass_2 && pass_2 > 5) {
                $('.error2').removeClass('active').fadeOut(200);
                $('.thumbs').css('transform', 'rotate(-90deg)');
                $('#pass1, #pass2').css('border', '#dce0e4 1px solid');
            } else {
                $('.error2').addClass('active').fadeIn(200);
                $('.thumbs').css('transform', 'rotate(0deg)');
                $('#pass1, #pass2').css('border', 'red 1px solid');
            }
//        })
    });
    //конец валидации--------------------------------------------------------------

    //блок_фильтр----------------------------------------------------------------
    $(".filter").on("click", ".btn_filter", function() {
        $(".filter .btn_filter").removeClass("active"); //удаляем класс во всех вкладках
        $(this).addClass("active"); //добавляем класс текущей (нажатой)
    });
    //конец блок_фильтра---------------------------------------------------------

});
//конец документа---------------------------------------------------------
